<?php
if (!function_exists('humanReadableNumber')){
    function humanReadableNumber($number, $decimalPlaces = null, $language = 'fa'){
        if (gettype($number) == 'integer'){
            return
                numLang(separator($number, false, $language, $decimalPlaces), $language);
        }else{
            $result = $decimalPlaces == null ? $number + 0 : round($number, $decimalPlaces);
            return
                numLang(separator($number, true, $language, $decimalPlaces), $language);
        }
    }
}
if (!function_exists('separator')){
    function separator($number,bool $decimal, $lang, $decimalPlaces){
        return
            decimal($number, $decimal, $lang, $decimalPlaces);
    }
}
if (!function_exists('decimal')){
    function decimal($number,bool $decimal, $lang, $intDecimal = null){
        $langSep = ['fa' => [',', '٬'], 'en' => ['.', ',',]];
        if ($decimal == false){
            return number_format($number, 0,
                $langSep[$lang][0], $langSep[$lang][1]);
        }else{
            if ($intDecimal == null){
                return number_format($number, countFloat($number),
                    $langSep[$lang][0], $langSep[$lang][1]);
            }else{
                return number_format($number, $intDecimal,
                    $langSep[$lang][0], $langSep[$lang][1]);
            }
        }
    }
}

if (!function_exists('numLang')){
    function numLang($number, $lang){
        $fa = ['۰', '۱', '۲', '۳', '۴', '۵', '۶', '۷', '۸', '۹'];
        $en = range(0, 9);
        return
            $lang == 'fa' ? str_replace($en , $fa, $number) : $number;
    }
}
if (!function_exists('countDigit')){
    function countFloat($number)
    {
        $count = explode('.', $number);
        $res =  strlen((string) $count[1]);
        return $res;
    }
}

